#include "common.h"

static void tick(void);
static void touchenemy(Entity* other);

void initEnemy(char *line)
{

    enemy = malloc(sizeof(Entity));
    memset(enemy, 0, sizeof(Entity));
    stage.entityTail->next = enemy;
    stage.entityTail = enemy;

    sscanf(line, "%*s %f %f", &enemy->x, &enemy->y);

    enemy->health = 1;
    

    enemy->texture = loadTexture("gfx/enemy.png");
    SDL_QueryTexture(enemy->texture, NULL, NULL, &enemy->w, &enemy->h);
    enemy->flags = EF_WEIGHTLESS;
    enemy->tick = tick;
    enemy->touch = touchenemy;

    
}
void doenemy(void)
{
    Entity* e,* prev;
    int adj;
    prev = &stage.entityHead;
    for (e = stage.entityHead.next; e != NULL; e = e->next)
    {
        self = e;
        if (e != player && collision(e->x, e->y, e->w, e->h, player->x, player->y, player->w, player->h))
        {
            if (e->touch) {
                e->touch(player);
            }
        }
        if (e->tick)
            e->tick;
        if (e->health <= 0)
        {
            if (e == stage.entityTail)
            {
                stage.entityTail = prev;
            }

            prev->next = e->next;
            free(e);
            e = prev;
        }

        prev = e;
    }
}


void resetGame(void)
{
    initStage();
    initGame();
}
static void tick(void)
{
    //self->x += ENEMY_SPEED;
    self->value += 0.1;

    self->x += sin(self->value);
    // If the enemy goes off-screen, wrap around to the other side
    if (self->x > SCREEN_WIDTH) {
        self->x = -self->w;
    }
}

static void touchenemy(Entity* other)
{
    if (other == player && player->health > 0)
    {
        self->health=0;
        player->health--;
        player->x = player->y = 0;
    }
}